Ensure the following dependencies are met by running
```bash
sudo apt install -y python-pip python-dev
sudo pip install scipy numpy pandas 
```

To run the output for project 1, do the following
1. Build catkin workspace
2. Source the devel/source.sh file
3. Launch the following launch file:
```bash
$ roslaunch vehicle_sim visualization.launch
```
