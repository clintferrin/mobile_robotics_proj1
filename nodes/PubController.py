#! /usr/bin/env python

import Parameters
import rospy
from numpy import array
from vehicle_sim.msg import vehicle_inputs, bicycle_state, velocity_trajectory, path_trajectory
from VelocityController import VeclocityController
from utils import Trajectory2D, Trajectory1D


def main():
    params = Parameters.Parameters()
    controller = VeclocityController(params)
    pub_controller = PubController(controller, params)
    pub_controller.publish(hz=params.pub_controller_freq)

    # spin() simply keeps python from exiting until this node is stopped


class PubController:
    def __init__(self, controller, params):
        self.controller = controller
        self.x_hat = array([[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]])
        self.new_velocity_trajectory = False
        self.new_position_trajectory = False

        self.velocity_trajectory = Trajectory1D(ds=params.dt)
        self.pos_trajectory = Trajectory2D(ds=params.ds)

        rospy.init_node('controller', anonymous=True)
        rospy.Subscriber("x_state", bicycle_state, self.__x_state_callback)
        rospy.Subscriber("velocity_plan", velocity_trajectory, self.__velocity_plan_callback)
        rospy.Subscriber("position_plan", path_trajectory, self.__position_plan_callback)

    def __x_state_callback(self, data):
        self.x_hat[0, 0] = data.x
        self.x_hat[1, 0] = data.y
        self.x_hat[2, 0] = data.psi
        self.x_hat[3, 0] = data.v
        self.x_hat[4, 0] = data.phi
        self.x_hat[5, 0] = data.b_v
        self.x_hat[6, 0] = data.b_phi

    def __velocity_plan_callback(self, data):
        self.velocity_trajectory.path = array(data.trajectory)
        self.velocity_trajectory.path_dot = array(data.trajectory_dot)
        self.controller.set_trajectory(self.velocity_trajectory)
        self.new_velocity_trajectory = True

    def __position_plan_callback(self, data):
        self.pos_trajectory.x.path = array(data.x)
        self.pos_trajectory.x.path_dot = array(data.x_dot)
        self.pos_trajectory.x.path_ddot = array(data.x_ddot)
        self.pos_trajectory.y.path = array(data.y)
        self.pos_trajectory.y.path_dot = array(data.y_dot)
        self.pos_trajectory.y.path_ddot = array(data.y_ddot)
        self.new_position_trajectory = True

    def publish(self, hz=100):
        pub = rospy.Publisher('control_input', vehicle_inputs, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        u = vehicle_inputs()
        while not rospy.is_shutdown():
            x_hat = self.x_hat.copy()
            u.accel = self.controller.calc_input(x_hat[3, 0])
            u.header.stamp = rospy.get_rostime()
            log_msg = "\nVehicle input: \n"
            log_msg = log_msg + "   throttle: " + str(u.accel)
            log_msg = log_msg + "   steering: " + str(u.change_steering_angle)
            rospy.loginfo(log_msg)
            pub.publish(u)
            rate.sleep()


if __name__ == '__main__':
    main()
