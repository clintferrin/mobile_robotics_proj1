#! /usr/bin/env python
# coding=utf-8

import numpy as np
from numpy import cos, sin, tan, sqrt
import utils
import Parameters
import rospy
import vehicle_sim.msg as vs
from tf.transformations import quaternion_from_euler
import geometry_msgs.msg as geo
import tf2_ros
import sys
import time


def main():
    if len(sys.argv) == 1:
        tf_parent = "origin"  # parent frame
        tf_child = "vehicle"  # child frame

    else:
        # validate argument parameters
        tf_parent = sys.argv[1]  # parent frame
        tf_child = sys.argv[2]  # child frame

    params = Parameters.Parameters()
    sim = VehicleSim(params)
    # sim.ros_sim(tf_parent=tf_parent, tf_child=tf_child)
    x = sim.propagate([1, 1])


class VehicleSim:
    def __init__(self, params):
        self.L = params.wheel_base
        self.steer_angle_limit = params.steer_angle_limit
        self.k_max = 1 / (self.L * np.tan(self.steer_angle_limit))  # TODO: correct?
        self.turn_rate_limit = params.turn_rate_limit  # per second
        self.eps = params.control_reference  # from back axel
        self.tau_v = params.tau_v
        self.tau_phi = params.tau_phi
        self.dt = params.dt  # integration time constant
        self.distance_traveled = 0

        # calculate sensor noise
        self.v_b_sig_init = params.v_b_sig_init
        self.phi_b_sig_init = params.phi_b_sig_init

        self.v_b_sig_ss = params.v_b_sig_ss
        self.phi_b_sig_ss = params.phi_b_sig_ss
        self.Q_v_b = 2 * self.v_b_sig_ss ** 2 / float(self.tau_v)
        self.Q_phi_b = 2 * self.phi_b_sig_ss ** 2 / float(self.tau_phi)
        self.Q_v_b_scale = sqrt(self.Q_v_b / self.dt)
        self.Q_phi_b_scale = sqrt(self.Q_phi_b / self.dt)

        if not params.noise:
            self.Q_v_b_scale = 0
            self.Q_phi_b_scale = 0
            self.v_b_sig_init = 0
            self.phi_b_sig_init = 0

        # set initial conditions
        self.x0 = np.zeros((7, 1))
        self.x0[0:5] = np.array(params.x0).reshape(-1, 1)
        self.x0[5:8] = np.array([np.random.normal() * self.v_b_sig_init,
                                 np.random.normal() * self.phi_b_sig_init]).reshape(-1, 1)
        self.x = self.x0
        self.u = np.array([[0], [0]])
        self.msg = vs.bicycle_state()

    def propagate(self, u):
        # calculate noise on sensors
        noise = np.array([np.random.normal() * self.Q_v_b_scale,
                          np.random.normal() * self.Q_phi_b_scale])

        self.x = utils.rk4(self.__x_dot, self.dt, self.x, u, noise)
        self.distance_traveled = self.distance_traveled + self.x[3, 0] * self.dt
        return self.x

    def __x_dot(self, x, u, noise):
        """
        :param u: system input (throttle, steer change)
        :param noise: measurement noise on bias sensors
        :return:  new state [ẋ₁ ẋ₂ ψ v Φ ḃ_v ḃ_Φ]'
        ┌    ┐   ┌               ┐
        │ ẋ₁ │   │    v⋅cos(ψ)   │  x position
        │ ẋ₂ │   │    v⋅sin(ψ)   │  y position
        │ ψ  │   │   v/L⋅tan(Φ)  │  heading
        │ v  │ = │       a       │  velocity
        │ Φ  │   │       ξ       │  steering angle
        │ ḃv │   │  -1/τv⋅bv+wv  │  velocity bias
        │ ḃΦ │   │  -1/τv⋅bv+wv  │  steer angle bias
        └    ┘   └               ┘
        """
        x_dot = np.array(
            [[x[3, 0] * cos(x[2, 0])],
             [x[3, 0] * sin(x[2, 0])],
             [x[3, 0] / float(self.L) * tan(x[4, 0])],
             [u[0]],
             [u[1]],
             [-1 / self.tau_v * x[5, 0] + noise[0]],
             [-1 / self.tau_phi * x[6, 0] + noise[1]]])

        return x_dot

    def __package_msg(self, x):
        self.msg.header.stamp = rospy.Time.now()
        self.msg.x = x[0, 0]
        self.msg.y = x[1, 0]
        self.msg.psi = x[2, 0]
        self.msg.v = x[3, 0]
        self.msg.phi = x[4, 0]
        self.msg.b_v = x[5, 0]
        self.msg.b_phi = x[6, 0]
        q = quaternion_from_euler(0, 0, x[2, 0])
        self.msg.pose.orientation.x = q[0]
        self.msg.pose.orientation.y = q[1]
        self.msg.pose.orientation.z = q[2]
        self.msg.pose.orientation.w = q[3]
        self.msg.pose.position.x = x[0, 0]
        self.msg.pose.position.y = x[1, 0]
        self.msg.pose.position.z = 0
        return self.msg

    def ros_sim(self, tf_parent="origin", tf_child="vehicle"):
        pub_x = rospy.Publisher('x_state', vs.bicycle_state, queue_size=10)

        rospy.init_node('vehicle_sim', anonymous=True)
        rate = rospy.Rate(100)  # hz

        while not rospy.is_shutdown():
            u = self.u
            x = self.propagate(u)
            log_str = "\nTime: %s" % rospy.get_time() + \
                      "\ninput: " + str(self.u.T) + "\n" + \
                      "state: \n" + str(self.x) + "\n"
            # rospy.loginfo(log_str)
            x_msg = self.__package_msg(x)

            pub_x.publish(x_msg)
            rate.sleep()


if __name__ == '__main__':
    main()
