from numpy import cos, sin, array, array
import numpy as np
import matplotlib.pyplot as plt

g = 9.80665


def rk4(fun, dt, x_old, input_1, input_2):
    # runge kutta 45
    h = dt
    hh = h / 2.
    h6 = h / 6.

    y = x_old
    dy_dx = fun(y, input_1, input_2)
    yt = y + hh * dy_dx

    dyt = fun(yt, input_1, input_2)
    yt = y + hh * dyt

    dym = fun(yt, input_1, input_2)
    yt = y + h * dym
    dym = dyt + dym

    dyt = fun(yt, input_1, input_2)
    return y + h6 * (dy_dx + dyt + 2 * dym)


def rotation_mat(theta):
    R = array([[cos(theta), -sin(theta)],
               [sin(theta), cos(theta)]])
    return R


def calc_resistive_force(v, params):
    v_km_h = v * 60 * 60 / 1000
    Cr = 0.005 + 1 / float(params.tire_pressure) * \
         (0.01 + .0095 * (v_km_h / 100) ** 2)  # rolling resistance
    return params.mass * g * Cr * cos(params.driving_incline) \
           + 1 / 2.0 * params.air_density * params.drag_coefficient * params.front_area * v ** 2 \
           + params.mass * g * sin(params.driving_incline)


def calc_resistive_accel(v, params):
    resistive_force = calc_resistive_force(v, params)
    return resistive_force / params.mass


def calc_power(v, accel, params):
    # incules regenerative breaking
    resistive_power = calc_resistive_force(v, params) * v
    acceleration_power = params.mass_factor * params.mass * accel * v
    return acceleration_power + resistive_power


def downsample_array(arr, points=500):
    step = int(round(len(array(arr)) / float(points)))
    if step < 1:
        return arr
    return arr[0:-1:step]


class Trajectory1D:
    def __init__(self, ds=.01):
        self.path = np.array([])
        self.path_dot = np.array([])
        self.path_ddot = np.array([])
        self.ds = ds


class Trajectory2D:
    def __init__(self, ds=.01):
        self.x = Trajectory1D(ds=ds)
        self.y = Trajectory1D(ds=ds)
        self.ds = ds
        self.theta = Trajectory1D(ds=ds)
        self.k = Trajectory1D(ds=ds)
        self.s = np.array([])
        self.s_geo = 0


def plot_line(p1, p2, ax=None, color=None):
    p1 = np.array(p1).squeeze()
    p2 = np.array(p2).squeeze()
    if ax is None:
        if color is None:
            plt.plot([p1[0], p2[0]], [p1[1], p2[1]],
                     linewidth=1)
        else:
            plt.plot([p1[0], p2[0]], [p1[1], p2[1]],
                     color=color,
                     linewidth=1)
    else:
        if color is None:
            ax.plot([p1[0], p2[0]], [p1[1], p2[1]],
                    linewidth=1)
        else:
            ax.plot([p1[0], p2[0]], [p1[1], p2[1]],
                    color=color,
                    linewidth=1)


def plot_vector(v1, ax=None, color=None):
    v1 = np.array(v1).squeeze()
    if ax is None:
        if color is None:
            plt.plot([0, v1[0]], [0, v1[1]],
                     linewidth=1)
        else:
            plt.plot([0, v1[0]], [0, v1[1]],
                     color=color,
                     linewidth=1)
    else:
        if color is None:
            ax.plot([0, v1[0]], [0, v1[1]],
                    linewidth=1)
        else:
            ax.plot([0, v1[0]], [0, v1[1]],
                    color=color,
                    linewidth=1)


def find_2D_error_components(pt_ref, pt_error, theta):
    x_ref = np.cos(theta)
    y_ref = np.sin(theta)
    ref = np.array([[x_ref], [y_ref]], dtype=float)
    ref_perp = np.array([[-ref[1, 0]], [ref[0, 0]]], dtype=float)
    error = pt_error - pt_ref

    x_error = np.dot(error.T, ref)
    y_error = np.dot(error.T, ref_perp)
    return x_error[0, 0], y_error[0, 0]


def wrap_to_pi(theta):
    return np.mod(theta + np.pi, 2 * np.pi) - np.pi
